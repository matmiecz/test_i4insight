from test_i4insight.cases import CasesList, Case


def test_read_cases(small_input_cases):
    cases = small_input_cases
    assert isinstance(cases, CasesList)
    assert all([isinstance(x, Case) for x in cases.cases])


def test_answer_question(single_case):
    answer = single_case.answer_question(0)
    assert isinstance(answer, int)


def test_solve_case(small_input_cases):
    cases = small_input_cases
    result = cases.solve()
    assert isinstance(result, dict)


def test_save_case(small_input_cases):
    input_case = "/work/data/single_case.in"
    cases = CasesList(input_case)
    result = cases.solve()
    target_path = "/work/data/single_case_output.data"
    cases.save(result, target_path)
