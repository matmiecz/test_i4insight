from test_i4insight.algorithm import find_time


def test_find_paths(single_case):
    q = single_case.questions[3]

    from test_i4insight.algorithm import find_paths

    result = find_paths(single_case.connections, 1, q.target_city)
    assert isinstance(result, list)


def test_find_times(single_case):
    q = single_case.questions[3]
    from test_i4insight.algorithm import find_paths

    paths = find_paths(single_case.connections, 1, q.target_city)
    example_path = paths[0]

    time = find_time(connections=single_case.connections, path=example_path, departure_time=q.departure_time)

    assert isinstance(time, int)
