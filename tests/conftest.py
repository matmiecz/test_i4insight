import pytest

from test_i4insight.cases import CasesList


@pytest.fixture
def small_input_cases():
    path = "/work/data/Small input.in"
    cases = CasesList(path)
    return cases


@pytest.fixture
def single_case(small_input_cases):
    return small_input_cases.cases[0]
