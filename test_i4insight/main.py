"""Main executable to compute and save results for both Large and Small input"""
from test_i4insight.cases import CasesList


def run_case(input_path, output_path):
    cases = CasesList(input_path)
    result = cases.solve()
    cases.save(result, output_path)


def _main():
    cases = {
        "small": {
            "data_in": "/work/data/Small input.in",
            "data_out": "/work/data/Small input.out",
        },
        "large": {
            "data_in": "/work/data/Large input.in",
            "data_out": "/work/data/Large input.out",
        },
    }

    for case_name, case_datapaths in cases.items():
        print(f"Running case: {case_name}")
        run_case(case_datapaths["data_in"], case_datapaths["data_out"])
        print(f"Finished case: {case_name}")


if __name__ == "__main__":
    _main()
