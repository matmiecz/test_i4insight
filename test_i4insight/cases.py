from collections import namedtuple

from test_i4insight.algorithm import find_time, find_paths


def _strip_and_split_line(line, sep=" "):
    """Extract list of integers from line"""
    return [int(x) for x in line.strip().split(sep)]


question = namedtuple("question", "target_city departure_time")


class Case:
    def __init__(self, connections, questions):
        self.connections = connections
        self.questions = questions

    def answer_question(self, question_id):
        q = self.questions[question_id]
        paths = find_paths(self.connections, 1, q.target_city)
        times = [find_time(connections=self.connections, path=path, departure_time=q.departure_time) for path in paths]
        if len(times) == 0:
            return -1
        else:
            min_time = min(times)
            return min_time


class CasesList:
    def __init__(self, path):
        self.cases = _read_input(path)

    def solve(self):
        """Return dictionary with results (case:time)"""
        result = {}
        for case_id, case in enumerate(self.cases):
            result[case_id] = {}
            for question_id, _ in enumerate(case.questions):
                result[case_id][question_id] = case.answer_question(question_id)
        return result

    def save(self, solution, target_file):
        header_text = "Case #{case_id}: "

        lines = []
        for case_id, results in solution.items():
            header = header_text.format(case_id=str(case_id))
            answers = " ".join((str(x) for x in results.values()))
            to_save = " ".join((header, answers, "\n"))
            lines.append(to_save)

        with open(target_file, 'w') as f:
            f.writelines(lines)


def _read_input(path):
    """
    The first line of the input gives the number of test cases, T.

    :param path to data file
    :return: list of Case objects
    """
    cases = []

    with open(path, "r") as f:
        # first line is number of cases
        n_cases = int(f.readline().strip())

        for i in range(n_cases):
            connections = {}
            questions = []

            # first line of each test case is N - cities; M - roads; K - questions
            l = f.readline()
            # N,M,K = [int(x) for x in l.strip().split(" ")]
            N, M, K = _strip_and_split_line(l)
            # M x 2 lines has to be read
            for i in range(M):
                city0, city1 = _strip_and_split_line(next(f))
                times = _strip_and_split_line(next(f))
                if connections.get(city0) is None:
                    connections[city0] = {}
                connections[city0][city1] = times

            for i in range(K):
                q = _strip_and_split_line(next(f))
                questions.append(question(*q))

            new_case = Case(connections=connections, questions=questions)
            cases.append(new_case)

    return cases
