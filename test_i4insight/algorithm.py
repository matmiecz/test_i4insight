def find_paths(connections, start_city, target_city):
    """Find path between start_city and target_city and return list of connection nodes"""
    visited_list = [[]]

    def find(connections, start_city, target_city, visited):
        visited.append(start_city)
        if start_city == target_city:
            visited_list.append(visited)
            return
        try:
            for next_city in connections[start_city]:
                if next_city not in visited:
                    find(connections, next_city, target_city, visited.copy())
            visited_list.append(visited)
        except KeyError:
            # print("Not possible to find a way")  # should be logged or dumped
            pass

    find(connections, start_city, target_city, [])
    paths_found = [x for x in visited_list if (len(x) > 1 and x[0] == start_city and x[-1] == target_city)]
    return paths_found


def find_time(connections, path, departure_time):
    """Find time from point A to B given connections dict and path (list)"""
    if len(path) < 2:
        return []

    current_time = departure_time
    accumated_time = 0
    for i in range(len(path) - 1):
        start_node = path[i]
        target_node = path[i + 1]

        t = connections[start_node][target_node][current_time]
        accumated_time += t
        current_time = (current_time + t) % 24

    return accumated_time
